package com.example.rt.ledcontrol.bluetoothConection;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.rt.ledcontrol.R;
import com.example.rt.ledcontrol.utils.Utils;


/**
 * @desc Esta actividad muestra una lista de todos los dispositivos que estan Paireados para
 * conectarse con alguno de ellos, esta es la interfaz que se muestra al usuario
 * @author Arturo Rivera Flores - A01202457
 * @required BluetoothState, DeviceList
 */


public class DeviceList extends Activity implements BluetoothConectionManager.BluetoothConnectionListener {

    /*
     * Variables for debugging
     */
    private static final String TAG = "Device List";
    private static final boolean DEBUG = true;

    // Member fields
    private BluetoothConectionManager btManager = BluetoothConectionManager.BLUETOOTH_MANAGER;
    private ArrayAdapter<String> mPairedDevicesArrayAdapter;
    private Button scanButton;

    private ProgressDialog ringProgressDialog = null;
    private ProgressDialog ringProgressDialog2 = null;

    private Activity activity;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        btManager.initBluetooth();
        btManager.setmBluetoothConectionListener(this);

        if(btManager.getState() == BluetoothState.STATE_CONNECTED){
            Intent intent = new Intent();
            // Set result and finish this Activity
            setResult(Activity.RESULT_OK, intent);
            finish();
        }

        if(DEBUG){
            Log.e(TAG, "Lista Iniciada");
        }

        // Setup the window
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        int listId = getIntent().getIntExtra("layout_list", R.layout.bluetooth_device_list);
        setContentView(listId);

        String strBluetoothDevices = getIntent().getStringExtra("bluetooth_devices");

        if(strBluetoothDevices == null){
            strBluetoothDevices = "Bluetooth Devices";
        }
        setTitle(strBluetoothDevices);

        // Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);

        // Initialize the button to perform device discovery
        scanButton = (Button) findViewById(R.id.button_scan);
        String strScanDevice = "Añadir Dispositivo";
        scanButton.setText(strScanDevice);
        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentBluetooth = new Intent();
                intentBluetooth.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                startActivity(intentBluetooth);            }
        });

        // Initialize array adapters. One for already paired devices
        // and one for newly discovered devices
        int layout_text = getIntent().getIntExtra("layout_text", R.layout.bluetooth_device_name);
        mPairedDevicesArrayAdapter = new ArrayAdapter<String>(this, layout_text);

        // Find and set up the ListView for paired devices
        ListView pairedListView = (ListView) findViewById(R.id.list_devices);
        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);

        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);

        activity = this;

        // If there are paired devices, add each one to the ArrayAdapter
        init();
    }

    public void init()
    {
        if(ringProgressDialog2 != null)
        {
            ringProgressDialog2.dismiss();
            ringProgressDialog2 = null;
        }
        mPairedDevicesArrayAdapter.clear();
        if (btManager.getPairedDeviceName().length > 0) {
            for (int i = 0; i< btManager.getPairedDeviceName().length ; i++ )
            {
                mPairedDevicesArrayAdapter.add(btManager.getPairedDeviceName()[i] + "\n" + btManager.getPairedDeviceAddress()[i]);
            }
        } else {
            String noDevices = "No devices found";
            mPairedDevicesArrayAdapter.add(noDevices);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        // Make sure we're not doing discovery anymore


        // Unregister broadcast listeners
        this.unregisterReceiver(mReceiver);
        this.finish();
    }

    // The on-click listener for all devices in the ListViews
    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            // Cancel discovery because it's costly and we're about to connect

            String strNoFound = getIntent().getStringExtra("no_devices_found");
            if(strNoFound == null)
                strNoFound = "No devices found";
            if(!((TextView) v).getText().toString().equals(strNoFound)) {
                // Get the device MAC address, which is the last 17 chars in the View
                String info = ((TextView) v).getText().toString();
                String address = info.substring(info.length() - 17);

                btManager.connect(address);
            }
        }
    };

    // The BroadcastReceiver that listens for discovered devices and
    // changes the title when discovery is finished
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    String strNoFound = getIntent().getStringExtra("no_devices_found");
                    if(strNoFound == null)
                        strNoFound = "No devices found";

                    if(mPairedDevicesArrayAdapter.getItem(0).equals(strNoFound)) {
                        mPairedDevicesArrayAdapter.remove(strNoFound);
                    }
                    mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                }

                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setProgressBarIndeterminateVisibility(false);
                String strSelectDevice = getIntent().getStringExtra("select_device");
                if(strSelectDevice == null)
                    strSelectDevice = "Select a device to connect";
                setTitle(strSelectDevice);
            }
        }
    };

    @Override
    public void onDeviceConnected(String name, String address)
    {
        if(ringProgressDialog != null)
        {
            ringProgressDialog.dismiss();
        }

        // Create the result Intent and include the MAC address
        Intent intent = new Intent();
        intent.putExtra(BluetoothState.EXTRA_DEVICE_ADDRESS, address);

        // Set result and finish this Activity
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onDeviceConecting() {
        this.runOnUiThread(new Runnable() {
            public void run() {
                ringProgressDialog = ProgressDialog.show(activity, "Espere ...", "Conectando ...", true);
            }
        });
    }

    @Override
    public void onDeviceConnectionFailed()
    {
        this.runOnUiThread(new Runnable() {
            public void run()
            {
                if(ringProgressDialog != null )
                {
                    ringProgressDialog.dismiss();
                    ringProgressDialog = null;
                }
                ringProgressDialog2 = ProgressDialog.show(activity, "Error al Conectar ...",	"Reiniciando ...", true);
            }
        });
        btManager.restartBluetooth();
        Utils.delay(1000);
        // Create the result Intent and include the MAC address
        Intent intent = new Intent();
        // Set result and finish this Activity
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
    }

}
