package com.example.rt.ledcontrol.bluetoothConection;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.example.rt.ledcontrol.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

/**
 * @desc Esta clase contiene todas las funciones para lograr tener una conexión por medio de Bluetooth
 * con otro dispositivo. Tiene los metodos e interfaces para enviar y recibir información.
 * @author Arturo Rivera Flores - A01202457
 * @required BluetoothState, DeviceList
 */
public class BluetoothConectionManager
{
    // Variables para el debuguear

    private static final boolean DEBUG = false;
    private static final String TAG = "BLUETOOTH CONECTION";



    // Se crea una instancia estatica para que toda la aplicación utilize  unicamente un BLUETOOTH_MANAGER

    public static BluetoothConectionManager BLUETOOTH_MANAGER= new BluetoothConectionManager();



    // Interfaz para monitorear el estado de Bluetooth, estos estados se encuentran en la clase BluetoothState

    private BluetoothStateListener mBluetoothStateListener = null;

    public interface BluetoothStateListener
    {
        public void onServiceStateChanged(int state);
    }



    // Interfaz para monitorear la conexion de bluetooth con otro dispositivo

    private BluetoothConnectionListener mBluetoothConectionListener = null;

    public interface BluetoothConnectionListener {
        public void onDeviceConnected(String name, String address);
        public void onDeviceConecting();
        public void onDeviceConnectionFailed();
    }


    // Interfaz que sirve para avisar y obtener la información que se recibe por medio de bluetooth

    private BluetoothRecieveListener mBluetoothRecieveListener = null;

    public interface BluetoothRecieveListener
    {
        public void onDataRecieve(ArrayList<Integer> array);
    }


    // UUID unico para esta aplicacion
    private static final UUID UUID_OTHER_DEVICE = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");


    // Threads que se utilizan para la comunicacion y conexion de bluetooth
    private ConnectThread mConnectThread;
    private AcceptThread mSecureAcceptThread;
    private ConnectedThread mConnectedThread;


    private static final String NAME_SECURE = "Bluetooth Secure";


    private BluetoothAdapter mAdapter;

    private int mState;



    /**
     * @desc Apaga y vuelve a prender el bluetooth enc aso de que exista algun error
     * @param
     * @return void
     */

    public void restartBluetooth()
    {
        mAdapter.disable();
        Utils.delay(3000);
        mAdapter.enable();
        Utils.delay(3000);
    }


    /**
     * @desc Constructor, inicializa los elementos de esta clase
     * @param
     * @return BluetoothConectionManager
     */

    public BluetoothConectionManager()
    {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = BluetoothState.STATE_NONE;
        mAdapter = BluetoothAdapter.getDefaultAdapter();
    }



    /**
     * @desc: Añadir la interfaz para monitorear el estado del bluetooth
     * @param: :BluetoothStateListener listener
     * @return: void
     */

    public void setBluetoothStateListener(BluetoothStateListener listener)
    {
        this.mBluetoothStateListener = listener;
    }



    /**
     * @desc: Añadir la interfaz para monitorear el estado de la conexion de bluetooth
     * @param: BluetoothConnectionListener listener
     * @return:
     */

    public void setmBluetoothConectionListener(BluetoothConnectionListener listener)
    {
        this.mBluetoothConectionListener = listener;
    }



    /**
     * @desc: Añadir la interfaz para recibir los mensajes que ser reciben por bluetooth
     * @param: BluetoothRecieveListener listener
     * @return:
     */
    public void setBluetoothRecieveListener(BluetoothRecieveListener listener)
    {
        this.mBluetoothRecieveListener = listener;
    }



    /**
     * @desc: Prende el bluetooth del dispositivo en caso de existir
     * @param: -
     * @return: true - si se pudo prender el bluetooth en el dispositivo o ya se encontraba prendido
     *          false - si no se pudo prender
     */
    public boolean initBluetooth()
    {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return false;
        }
        if (!mBluetoothAdapter.isEnabled())
        {
            mBluetoothAdapter.enable();
            Utils.delay(3000);
        }
        return true;
    }



    /**
     * @desc: Obtiene una lista de strings con los nombres de los dispositivos que estan "paired" con el telefono
     * @param: -
     * @return: Stiring [] - Una lista con los nombres de los dispositivos
     */
    public String[] getPairedDeviceName() {
        int c = 0;
        Set<BluetoothDevice> devices = mAdapter.getBondedDevices();
        String[] name_list = new String[devices.size()];
        for(BluetoothDevice device : devices) {
            name_list[c] = device.getName();
            c++;
        }
        return name_list;
    }



    /**
     * @desc: Obtiene una lista de strings con las direcciones de los dispositivos que estan "paired" con el telefono
     * @param: -
     * @return: Stiring [] - Una lista con las direcciones de los dispositivos
     */
    public String[] getPairedDeviceAddress() {
        int c = 0;
        Set<BluetoothDevice> devices = mAdapter.getBondedDevices();
        String[] address_list = new String[devices.size()];
        for(BluetoothDevice device : devices) {
            address_list[c] = device.getAddress();
            c++;
        }
        return address_list;
    }


    /**
     * @desc: Obtiene el estado en el que se encuentar el bluetooth, para ver que significa cada estado
     *          ver la clase BluetoothState
     * @param: void
     * @return: int - estado en el que se encuentra el dispositivo
     */

    public synchronized int getState()
    {
        return mState;
    }



    /**
     * @desc: Conecta el telefono con el dispositivo bluetooth dependiendo de su direccion
     * @param: Stirng address- direccion del dispositivo al que se quiere conectar
     * @return: void
     */
    public synchronized void connect(String address)
    {
        initBluetooth();
        BluetoothDevice device = mAdapter.getRemoteDevice(address);
        connect(device);
    }


    /**
     * @desc: Conecta el telefono con el dispositivo bluetooth en base al BluetoothDevice
     * @param: BluetoothDevice device - dispositivo al que se quiere contectar
     * @return: void
     */
    public synchronized void connect(BluetoothDevice device)
    {
        if(mBluetoothConectionListener != null) {
            mBluetoothConectionListener.onDeviceConecting();
        }

        // Cancel any thread attempting to make a connection
        if (mState == BluetoothState.STATE_CONNECTING)
        {
            if (mConnectThread != null)
            {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null)
        {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(BluetoothState.STATE_CONNECTING);
    }



    /**
     * @desc: Inicia el thread donde se ejecutara toda la interaccion via bluetooth
     * @param: void
     * @return: void
     */
    public synchronized void start()
    {
        if (mConnectThread != null)
        {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        // Cancel any thread currently running a connection
        if (mConnectedThread != null)
        {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(BluetoothState.STATE_LISTEN);
        if (mSecureAcceptThread == null) {
            mSecureAcceptThread = new AcceptThread();
            mSecureAcceptThread.start();
        }
    }


    /**
     * @desc: Termina el thread para dejar de utilizar la conexcion bluetooth
     * @param: void
     * @return: void
     */
    public synchronized void stop() {
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread.kill();
            mSecureAcceptThread = null;
        }
        setState(BluetoothState.STATE_NONE);

    }


    /**
     * @desc: Envia la informacion al dispositivo bluetooth
     * @param: byte[] data - los bytes que se enviaran al dispositivo bluetooth
     *          boolean CRLF - True si se quiere adicionar "Carriage Return" y "Line Feed" al final
     * @return: void
     */
    public void send(byte[] data, boolean CRLF) {
        if(getState() == BluetoothState.STATE_CONNECTED) {
            if(CRLF) {
                byte[] data2 = new byte[data.length + 2];
                for(int i = 0 ; i < data.length ; i++)
                    data2[i] = data[i];
                data2[data2.length - 0] = 0x0A;
                data2[data2.length] = 0x0D;
                write(data2);
            } else {
                write(data);
            }
        }
    }


    /**
     * @desc: Envia la informacion al dispositivo bluetooth
     * @param: String - Un string que se enviara al dispositivo bluetooth
     *          boolean CRLF - True si se quiere adicionar "Carriage Return" y "Line Feed" al final
     * @return: void
     */
    public void send(String data, boolean CRLF) {
        if(getState() == BluetoothState.STATE_CONNECTED) {
            if(CRLF)
                data += "\r\n";
            write(data.getBytes());
        }
    }



    /**
     * @desc: Utiliza un thread para mandar la información al dispositivo bluetooth
     * @param: byte[] out - los bytes que se van a enviar
     * @return: void
     */
    private void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != BluetoothState.STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);

    }


    /**
     * @desc: En caso de que la conexion falle, se reinicia el thread
     * @param: void
     * @return: void
     */
    private void connectionFailed() {
        if(mBluetoothConectionListener != null) {
            mBluetoothConectionListener.onDeviceConnectionFailed();
        }
        // Start the service over to restart listening mode
        BluetoothConectionManager.this.start();
    }


    /**
     * @desc: En caso de que se pierda la conexcion se reinicia el thread
     * @param: void
     * @return: void
     */
    private void connectionLost() {
        // Start the service over to restart listening mode
        BluetoothConectionManager.this.start();
    }

    /**
     * @desc: Modifica la bandera del estado de la conexion de bluetooth
     * @param: in state - El estado en el que se encuentra la conexion blueooth (Ver clase BluetoothState)
     * @return: void
     */
    private synchronized void setState(int state)
    {
        mState = state;
        // Give the new state to the Handler so the UI Activity can update
        if(mBluetoothConectionListener != null) {
            mBluetoothStateListener.onServiceStateChanged(state);
        }
    }




    /**
     * @desc: Clase que extiende de Thread para crear un thread separado al principal para conectarse con
     *      el dispositivo bluetooth deseado
     *
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;


        /**
         * @desc: Se crea un thread para iniciar la conexion con un BluetoothDevice
         * @param: BluetoothDevice device - el dispositivo al que se va a conectar
         * @return: void
         */
        public ConnectThread(BluetoothDevice device)
        {
            BluetoothSocket tmp = null;
            mmDevice = device;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try
            {
                //Method m = device.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
                //tmp = (BluetoothSocket) m.invoke(device, 1);
                tmp = device.createRfcommSocketToServiceRecord(UUID_OTHER_DEVICE);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mmSocket = tmp;
        }


        /**
         * @desc: Cofigo que se ejcuta cuando se corre el thread, Aqui se intenta establecer conexion con el dispositivo
         * @param: void
         * @return: void
         */
        public void run() {
            // Always cancel discovery because it will slow down a connection
            mAdapter.cancelDiscovery();


            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException e) {
                // Close the socket
                e.printStackTrace();
                try {
                    mmSocket.close();
                } catch (IOException e2)
                {
                    e.printStackTrace();
                }
                connectionFailed();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BluetoothConectionManager.this) {
                mConnectThread = null;
            }


            // Start the connected thread
            connected(mmSocket, mmDevice);
        }

        /**
         * @desc: Termina el thread y la interaccion de bluetooth
         * @param: void
         * @return: void
         */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

    /**
     * @desc: Termina el thread e inicia un thread que servira para interactuar con el dispositivo bluetooth
     * @param: void
     * @return: void
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice
            device)
    {
        // Cancel the thread that completed the connection
        if (mConnectThread != null)
        {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null)
        {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }


        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity

        setState(BluetoothState.STATE_CONNECTED);
        if(mBluetoothConectionListener != null) {
            mBluetoothConectionListener.onDeviceConnected(device.getName(), device.getAddress());
        }
    }

    /**
     * @desc: Este thread se ejecuta cuando se conecta con un dispositivo de manera exitosa
     *          Aqui se manejan el envio y recepcion de datos
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;


        /**
         * @desc: Inicia el socket por el cual se establecera la conexion bluetooth
         *
         */
        public ConnectedThread(BluetoothSocket socket)
        {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;

        }


        /**
         * @desc: Se ejecuta este ciclo hasta que se pierde la conexion que ya se establecio con el dispositivo
         *      aqui se la pasa buscando datos que le lleguen del dispositivo
         * @param: void
         * @return: void
         */
        public void run() {

            byte[] buffer;
            ArrayList<Integer> arr_byte = new ArrayList<Integer>();

            // Keep listening to the InputStream while connected
            while (true) {
                try {
                    int data = mmInStream.read();
                    if(DEBUG){
                        Log.e(TAG,"RECIBIDO (int): -"+data+"-");
                        Log.e(TAG,"(char): -"+(char)(data)+"-");
                    }

                    if(data == 0x0A) {
                    } else if(data == 0x0D) {
                        buffer = new byte[arr_byte.size()];
                        for(int i = 0 ; i < arr_byte.size() ; i++) {
                            buffer[i] = arr_byte.get(i).byteValue();
                        }
                        if(mBluetoothRecieveListener != null){
                            mBluetoothRecieveListener.onDataRecieve(arr_byte);
                        }
                        // Send the obtained bytes to the UI A
                        arr_byte = new ArrayList<Integer>();
                    } else {
                        arr_byte.add(data);
                    }
                } catch (IOException e) {
                    connectionLost();
                    // Start the service over to restart listening mode
                    BluetoothConectionManager.this.start();
                    break;
                }
            }
        }

        /**
         * @desc: Envia los datos al dispositivo conectado en un thread separado
         * @param: byte[] buffer - datos que se van a enviar
         * @return: void
         */
        public void write(byte[] buffer) {
            try {/*
                byte[] buffer2 = new byte[buffer.length + 2];
                for(int i = 0 ; i < buffer.length ; i++)
                    buffer2[i] = buffer[i];
                buffer2[buffer2.length - 2] = 0x0A;
                buffer2[buffer2.length - 1] = 0x0D;*/
                mmOutStream.write(buffer);
                mmOutStream.flush();
                if(DEBUG){
                    Log.e(TAG, "STRING A ENVIAR: " + new String(buffer) + "-");
                    Log.e(TAG, "BYTES: INICIO ");
                    for(int i = 0; i<buffer.length;i++)
                    {
                        Log.e(TAG,"ENVIADO: -"+buffer[i]+"-");
                    }
                    Log.e(TAG,"FIN");
                }

                // Share the sent message back to the UI Activity

            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }


        /**
         * @desc: Termina la comunicacion al dispositivo bluetooth
         * @param: void
         * @return: void
         */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * @desc: En caso de querer ocupar el dispositivo como el que solicita conectarse se utiliza este thread
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private BluetoothServerSocket mmServerSocket;
        private String mSocketType;
        boolean isRunning = true;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;

            // Create a new listening server socket
            try {
                tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE, UUID_OTHER_DEVICE);
            } catch (IOException e) { }
            mmServerSocket = tmp;
        }

        public void run() {
            setName("AcceptThread" + mSocketType);
            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != BluetoothState.STATE_CONNECTED && isRunning) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (BluetoothConectionManager.this) {
                        switch (mState) {
                            case BluetoothState.STATE_LISTEN:
                            case BluetoothState.STATE_CONNECTING:
                                // Situation normal. Start the connected thread.
                                connected(socket, socket.getRemoteDevice());
                                break;
                            case BluetoothState.STATE_NONE:
                            case BluetoothState.STATE_CONNECTED:
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close();
                                } catch (IOException e) { }
                                break;
                        }
                    }
                }
            }
        }

        public void cancel() {
            try {
                mmServerSocket.close();
                mmServerSocket = null;
            } catch (IOException e) { }
        }

        public void kill() {
            isRunning = false;
        }
    }
}
